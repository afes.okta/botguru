<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <h2>Zip File Upload and Extracting</h2>
                <form method='POST' enctype="multipart/form-data" action='<?= base_url();?>upload'>
                    <div class="form-group">
                        <div class="col-md-10">
                            <input type="file" name="file" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <input type="submit" name="fileUploadSubmit" value="UPLOAD & UNZIP"
                                   class="form-control btn btn-info">
                        </div>
                    </div>
                </form>
                <div style="margin:10px;font-weight:bolder;">
                    <i>
                        <?php
                        if(isset($_SESSION['msg'])) echo $_SESSION['msg'];
                        if(isset($_SESSION['error'])) echo $_SESSION['error'];
                        ?>
                    </i>
                </div>
            </div>
        </div>
    </section>
</div>